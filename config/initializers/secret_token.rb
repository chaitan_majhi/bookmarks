# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Bookmarks::Application.config.secret_key_base = 'a6f457ee6e129d79792b1f7789b32ed4cf7b2863c6ecbffe3581cba4e509eeaf5cd7aba7be1efd24820eb542c3e1a66ee519a1ceabbeb5b200736ecd7c7d5c6a'
